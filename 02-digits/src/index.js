function addTwoDigits(num) {
    const numbers = num.toString().split('');

    return numbers.reduce((total, value) => {
        return parseInt(total) + parseInt(value);
    }, 0);
}



/**
* Test Suite 
*/
describe('addTwoDigits()', () => {
    it('take a two digit number and return the sum of their numbers', () => {
        // arrange
        const num = 29;

        // act
        const result = addTwoDigits(num);

        // log
        console.log("result: ", result);

        // assert
        expect(result).toBe(11);
    });
});