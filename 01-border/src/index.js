function addBorder(array) {
    const message = "*".repeat(array[0].length + 2);

    // console.log(array[0].length);

    array.unshift(message);
    array.push(message);

    for (let i = 1; i < array.length - 1; i++) {
        array[i] = '*'.concat(array[i], '*');
    }

    // console.debug(array);

    return array;
}

/**
* Test Suite 
*/
describe('addBorder()', () => {
    it('adds a border around entire application', () => {
        // arrange
        const strings = ['abc', 'ded'];

        // act
        const result = addBorder(strings);

        // log
        console.log("result: ", result);

        // assert
        expect(result).toEqual([
            "*****",
            "*abc*",
            "*ded*",
            "*****"
        ]);
    })
});