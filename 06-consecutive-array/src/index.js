function makeArrayConsecutive(nums) {

    let count = 0;
    const array = nums.sort((a, b) => a - b);

    for (let i = nums[0]; i < nums[nums.length - 1]; i++) {
        if (!nums.includes(i)) {
            count++;
        }
    }

    return count;
}



/**
* Test Suite 
*/
describe('makeArrayConsecutive()', () => {
    it('returns total missing numbers between smallest and largest number', () => {
        // arrange
        const nums = [6, 2, 3, 8];

        // act
        const result = makeArrayConsecutive(nums);

        // log
        console.log("result: ", result);

        // assert
        expect(result).toBe(3);
    })
});