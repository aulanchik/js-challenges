function sumAllPrimes(num) {
    let primesSubtotal = 0;

    for (let i = 2; i < num; i++) {
        for (let j = 2; j <= i; j++) {
            if (i === j) {
                primesSubtotal += i;
            }

            if (i % j === 0) {
                break;
            }
        }
    }

    return primesSubtotal;
}

/**
* Test Suite 
*/
describe('sumAllPrimes()', () => {
    it('adds all prime numbers up to input number', () => {
        // arrange
        const num = 10;

        // act
        const result = sumAllPrimes(num);

        // log
        console.log("result: ", result);

        // assert
        expect(result).toBe(17);
    })
});