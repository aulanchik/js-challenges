function evenDigitsOnly(number) {
    const digits = number.toString().split('');

    // checks by the each digit if they are even or not
    const isEvenDigit = digits.every((digit) => parseInt(digit) % 2 === 0);

    return isEvenDigit;
}

/**
* Test Suite 
*/
describe('evenDigitsOnly()', () => {
    it('returns true when all digits are even', () => {
        // arrange
        const nums = 248622;

        // act
        const result = evenDigitsOnly(nums);

        // log
        console.log("result 1 : ", result);

        // asserta
        expect(result).toBe(true);
    });

    it('returns fale when any digits are odd', () => {
        // arrange
        const nums = 642386;

        // act
        const result = evenDigitsOnly(nums);

        // log
        console.log("result 2 : ", result);

        // assert
        expect(result).toBe(false);
    });
});